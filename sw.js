self.addEventListener('install', event =>{
    console.log('Se instala SW.. nuevo');
    //creacion del caché 
    caches.open('mi-app-shell').then(cache => {
        return cache.addAll([
            'index.html',
            '/css/style.css',
            '/js/app.js',
            '/img/bannerBolsaDeTrabajo.jpeg',
            '/img/UTH-LOGO-VERT.png',
            'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js'
        ]);
    });
});

self.addEventListener('activate', event =>{
    console.log('El SW se ha activado');
});

self.addEventListener('fetch', event =>{
    console.log(event.request.url);
    event.respondWith(
        caches.match(event.request)
    );
});